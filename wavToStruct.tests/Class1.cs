﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using NUnit.Framework;
using wavToStruct.Presenter;
using wavToStruct;
using wavToStruct.Model;

namespace wavToStruct.tests
{

    [TestFixture]
    public class Tests
    {
        class DumbSource: IWavFileLayer, IStructFileLayer
        {

            public string FileName { get; set; }
        
            public byte[] RiffId { get; set; }
        
            public uint Size { get; set; }
        
            public byte[] WavId { get; set; }
        
            public byte[] FmtId { get; set; }
        
            public uint FmtSize { get; set; }
        
            public ushort Format { get; set; }

            public ushort Channels { get; set; }

            public uint SampleRate { get; set; }

            public uint BytePerSecond { get; set; }

            public ushort BlockSize { get; set; }

            public ushort Bit { get; set; }

            public byte[] DataId { get; set; }

            public uint DataSize { get; set; }

            public short[] Data { get; set; }
            
            public event System.EventHandler<System.EventArgs> LoadWav;

            public string OutFileName { get; set; }

            public string StructName { get; set; }

            public int ArraySize { get; set; }

            public short RowSize { get; set; }

            public StructTypes StructTypes { get; set; }

            public List<string> StructData { get; set; }

            public bool ConvertEnabled { get; set; }
            
            public event EventHandler<EventArgs> SaveStruct;
        }


        private DumbSource dumbSource;

        [SetUp]
        public void MySetUp()
        {
            dumbSource = new DumbSource();

            //default data
            dumbSource.RowSize = 8;
            dumbSource.Data = new short[100];

            for (short i = 0; i < dumbSource.Data.Length; ++i)
            {
                dumbSource.Data[i] = (short)(i + 1);
            }            
            
        }

        [Test]
        public void TestConvertor()
        {
            WavConvertorPresenter presenter = new WavConvertorPresenter(dumbSource);
            presenter.Convert();

            Assert.AreEqual(12, dumbSource.StructData.Count);
            Assert.AreEqual("0x0001, 0x0002, 0x0003, 0x0004, 0x0005, 0x0006, 0x0007, 0x0008, 0x0009,",
                dumbSource.StructData[0].Trim());
            Assert.AreEqual("0x000A, 0x000B, 0x000C, 0x000D, 0x000E, 0x000F, 0x0010, 0x0011, 0x0012,",
                dumbSource.StructData[1].Trim());

            Assert.AreEqual("0x0064", dumbSource.StructData[11].Trim());

        }


    }
}
