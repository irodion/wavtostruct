﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace wavToStruct
{
    interface IWavFileLayer
    {
        string  FileName { get; set; }
        byte[] RiffId { get; set; }
        uint Size { get; set; }
        byte[] WavId { get; set; }
        byte[] FmtId { get; set; }
        uint FmtSize { get; set; }
        ushort Format { get; set; }
        ushort Channels { get; set; }
        uint SampleRate { get; set; }
        uint BytePerSecond { get; set; }
        ushort BlockSize { get; set; }
        ushort Bit { get; set; }
        byte[] DataId { get; set; }
        uint DataSize { get; set; }
        short[] Data  { get; set; }

        event EventHandler<EventArgs> LoadWav;
    }

}
