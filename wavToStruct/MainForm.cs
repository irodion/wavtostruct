﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading.Tasks;
using System.Windows.Forms;
using wavToStruct.Presenter;
using wavToStruct.Model;

namespace wavToStruct
{
    public partial class MainForm : Form, IWavFileLayer, IStructFileLayer
    {
        private string _wavFileName;
        private string _structFileName;

        private WavConvertorPresenter _presenter;

        public MainForm()
        {
            InitializeComponent();
            _presenter = new WavConvertorPresenter(this);
            comboBoxStructType.SelectedIndex = 0;
        }

        public string FileName
        {
            get { return (_wavFileName); }

            set { _wavFileName = value; }
        }

        public byte[] RiffId
        {
            get { return null; }

            set
            {
                Contract.Requires(null != value);
                labelRiff.Text = BitConverter.ToString(value);
            }

        }

        public new uint Size { get; set; }
        
        public byte[] WavId
        {
            get { return (null); }
            set
            {
                Contract.Requires(null != value); 

                labelWavFormat.Text = BitConverter.ToString(value); 
            }
        }

        public byte[] FmtId { get; set; }
        public uint FmtSize { get; set; }
        public ushort Format { get; set; }
        
        public ushort Channels
        {
            get { throw new NotImplementedException();}
            set { labelChannels.Text = value.ToString(); }
        }

        public uint SampleRate
        {
            get { return uint.Parse(labelSampleRate.Text); }
            set { labelSampleRate.Text = value.ToString(); }
        }

        public uint BytePerSecond
        {
            get { return uint.Parse(labelBytePerSecond.Text); }
            set { labelBytePerSecond.Text = value.ToString(); }
        }

        public ushort BlockSize
        {
            get { return (ushort.Parse(labelBlockSize.Text)); }
            set { labelBlockSize.Text = value.ToString();     }
        }

        public ushort Bit { get; set; }
        public byte[] DataId { get; set; }
        public uint DataSize
        {
            get { return (ushort.Parse(labelDataSize.Text)); }
            set { labelDataSize.Text = value.ToString();     }
        }

        public short[] Data { get; set; }

        public event EventHandler<EventArgs> LoadWav;

        public string OutFileName
        {
            
            get { return (_structFileName); }
            set
            {
                Contract.Requires(false == string.IsNullOrWhiteSpace(value));

                _structFileName = value;
            }
        }
        
        public string StructName { get; set; }
        public int ArraySize { get; set; }

        public short RowSize
        {
            get { return ((short)numericNumbersInRow.Value); } 
            set { numericNumbersInRow.Value = value; }
        }

        public StructTypes StructTypes
        {
            get { return ((StructTypes)Enum.ToObject(typeof(StructTypes), comboBoxStructType.SelectedIndex)); }

            set { comboBoxStructType.SelectedIndex = (int)value; }
        }

        public bool ConvertEnabled
        {
            get { return (buttonConvert.Enabled); }
            set { buttonConvert.Enabled = value; }
        }    

        public List<String> StructData { get; set; }

        public event EventHandler<EventArgs> SaveStruct;

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            Contract.Requires(null != LoadWav);

            if (DialogResult.OK == openFileDialog.ShowDialog())
            {
                FileName = openFileDialog.FileName;
                LoadWav(this, null);
                textBoxFileName.Text = FileName;
            } 
        }

        private void buttonChooseSaveFile_Click(object sender, EventArgs e)
        {
            // choose out file

            if (DialogResult.OK == saveFileDialog.ShowDialog())
            {
                OutFileName = saveFileDialog.FileName;
                textBoxOutFile.Text = OutFileName;
            }
        }

        private void buttonConvert_Click(object sender, EventArgs e)
        {
            Contract.Requires(null != SaveStruct);

            OutFileName = textBoxOutFile.Text;
            RowSize = (short)numericNumbersInRow.Value;
            StructTypes = (StructTypes)comboBoxStructType.SelectedIndex;
            StructName = textBoxStructName.Text;
            SaveStruct(this, null);
            //MessageBox.Show(@"Converted!", @"OK", MessageBoxButtons.OK);
        }


    }
}
