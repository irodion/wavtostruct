﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Text;
using System.Threading.Tasks;
using wavToStruct.Model;

namespace wavToStruct.Presenter
{
    class WavConvertorPresenter
    {
        //Wav file model
        private readonly WavFile _wavFile = new WavFile();
        //Struct file model
        private StructFile _structFile = new StructFile();

        public readonly IWavFileLayer viewWav;
        private readonly IStructFileLayer viewStruct;

        /// <summary>
        /// Presenter for converting data
        /// </summary>
        /// <param name="viewWav">Must implement interfaces IStructFileLayer and IWavFileLayer</param>
        public WavConvertorPresenter(Object viewWav)
        {
            Contract.Requires(null != viewWav);
            Contract.Requires(viewWav is IStructFileLayer);
            Contract.Requires(viewWav is IWavFileLayer);

            this.viewWav = viewWav as IWavFileLayer;
            viewStruct = viewWav as IStructFileLayer;

            this.viewWav.LoadWav += Load;
            this.viewStruct.SaveStruct += Save;
        }

        void Load(Object sender, EventArgs eventArgs)
        {
            _wavFile.LoadFromFile(viewWav.FileName);

            
            viewWav.Channels = _wavFile.Channels;
            viewWav.RiffId = _wavFile.RiffId;
            viewWav.WavId = _wavFile.WavId;
            viewWav.Data = _wavFile.Data;
            viewWav.BytePerSecond = _wavFile.BytePerSecond;
            viewWav.SampleRate = _wavFile.SampleRate;
            viewWav.BlockSize = _wavFile.BlockSize;
            viewWav.DataSize = _wavFile.DataSize;
            viewStruct.ConvertEnabled = true;
        }

        /// <summary>
        /// Convert data from wave format to string
        /// </summary>
        public void Convert()
        {
            Contract.Requires(null != viewWav.Data);

            _structFile.StructData = new List<string>();

            StringBuilder outString = new StringBuilder();
            short numbersInRowCount = 0;

            for (int i = 0; i < viewWav.Data.Length; ++i)
            {
                //convert short to hex string
                short data = viewWav.Data[i];

                if (0 == numbersInRowCount)
                {
                    outString.Append("\t");
                }

                outString.AppendFormat(Properties.Settings.Default.outFormat, data);
                
                if (numbersInRowCount >= viewStruct.RowSize)
                {
                    // end of one row
                    if (i != (viewWav.Data.Length - 1))
                    {
                        outString.AppendFormat(", ");    
                    }
                    
                    _structFile.StructData.Add(outString.ToString());
                    outString.Clear();
                    numbersInRowCount = 0;
                }
                else
                {
                    outString.AppendFormat(", ");
                    numbersInRowCount++;
                }
            }

            if (outString.Length > 0)
            {
                outString.Remove(outString.Length - 2, 1);
                _structFile.StructData.Add(outString.ToString());
                outString.Clear();
            }

            _structFile.ArraySize = viewWav.Data.Length;
            viewStruct.StructData = _structFile.StructData;
        }

        /// <summary>
        /// Save loaded data to struct
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void Save(Object sender, EventArgs eventArgs)
        {
            Contract.Requires(null != viewWav.Data);

            Convert();
            _structFile.StructName = viewStruct.StructName;
            _structFile.StructTypes = viewStruct.StructTypes;
            _structFile.RowSize = viewStruct.RowSize;
            _structFile.Channels = _wavFile.Channels;
            _structFile.SaveToFile(viewStruct.OutFileName);
        }

        [ContractInvariantMethod]
        private void InvariantObjects()
        {
            Contract.Invariant(null != viewWav);
            Contract.Invariant(null != viewStruct);
            Contract.Invariant(null != _wavFile);
            Contract.Invariant(null != _structFile);
        }

    }
}
