﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using NUnit.Framework.Constraints;

namespace wavToStruct.Model
{
    public enum StructTypes
    {
        TypeShort = 0x00,
        TypeUbyte = 0x01,
        TypeByte = 0x02
    }

    class StructFile 
    {
        public string OutFileName { get; set; }
        public string StructName { get; set; }
        public int ArraySize { get; set; }
        public short RowSize { get; set; }
        public ushort Channels { get; set; }
        public StructTypes StructTypes { get; set; }
        public List<string> StructData { get; set; } // Strings in file

        public bool SaveToFile(string fileName)
        {
            Contract.Assume(false == string.IsNullOrEmpty(fileName));
            Contract.Assume(null != StructData);

            OutFileName = fileName;

            using (StreamWriter file = new StreamWriter(OutFileName))
            {
                file.WriteLine(@"// array size " + ArraySize);
                file.WriteLine(@"// channels: " + Channels);
                file.WriteLine("static const {0} {1}[] = ", TypeToString(StructTypes), StructName);
                file.WriteLine("{");
                //write all strings
                foreach (var line in StructData)
                {
                    file.WriteLine(line);
                }

                file.WriteLine("}");
                file.WriteLine(" ");
                file.Flush();
                file.Close();
            }

            return (true);
        }

        private string TypeToString(StructTypes type)
        {

            Contract.Requires(Enum.IsDefined(typeof(StructTypes), type));
             
            switch (type)
            {
                case StructTypes.TypeUbyte:
                    return ("uint8_t");

                case StructTypes.TypeShort:
                    return ("int16_t");

                case StructTypes.TypeByte:
                    return ("uint16_t");

                default:
                    return ("");
            }
        }
    }
}
