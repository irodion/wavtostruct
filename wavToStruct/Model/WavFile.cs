﻿ using System;

using System.Diagnostics.Contracts;
using System.IO;

namespace wavToStruct.Model
{
    class WavFile
    {
        public string  FileName { get; set; }
        public byte[] RiffId { get; set; }
        public uint Size { get; set; }
        public byte[] WavId { get; set; }
        public byte[] FmtId { get; set; }
        public uint FmtSize { get; set; }
        public ushort Format { get; set; }
        public ushort Channels { get; set; }
        public uint SampleRate { get; set; }
        public uint BytePerSecond { get; set; }
        public ushort BlockSize { get; set; }
        public ushort Bit { get; set; }
        public byte[] DataId { get; set; }
        public uint DataSize { get; set; }
        public short[] Data  { get; set; }

        public bool LoadFromFile(String fileName)
        {
            Contract.Assume(false == string.IsNullOrEmpty(fileName));

            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader binaryReader = new BinaryReader(fileStream))
                {
                    try
                    {
                        FileName = fileName;
                        RiffId = binaryReader.ReadBytes(4);
                        Size = binaryReader.ReadUInt32();
                        WavId = binaryReader.ReadBytes(4);
                        FmtId = binaryReader.ReadBytes(4);
                        FmtSize = binaryReader.ReadUInt32();
                        Format = binaryReader.ReadUInt16();
                        Channels = binaryReader.ReadUInt16();
                        SampleRate = binaryReader.ReadUInt32();
                        BytePerSecond = binaryReader.ReadUInt32();
                        BlockSize = binaryReader.ReadUInt16();
                        Bit = binaryReader.ReadUInt16();
                        DataId = binaryReader.ReadBytes(4);
                        DataSize = binaryReader.ReadUInt32();

                        // read all data

                        long toRead = DataSize/BlockSize;
                        Data = new short[toRead];

                        for (long i = 0; toRead > i; ++i)
                        {
                            if (1 == Channels)
                            {
                                Data[i] = binaryReader.ReadInt16();
                            }
                            else if (2 == Channels)
                            {
                                throw (new NotImplementedException());
                            }
                        }

                        binaryReader.Close();
                    }
                    catch (IOException)
                    {
                        return (false);
                    }

                    return (true);
                }
                
            }

        }
    }
}
