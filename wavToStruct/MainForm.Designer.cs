﻿namespace wavToStruct
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupInputInfo = new System.Windows.Forms.GroupBox();
            this.labelWavFormat = new System.Windows.Forms.Label();
            this.labelWaveFormatLabel = new System.Windows.Forms.Label();
            this.labelChannels = new System.Windows.Forms.Label();
            this.labelChannelsLabel = new System.Windows.Forms.Label();
            this.labelRiff = new System.Windows.Forms.Label();
            this.labelRFFID = new System.Windows.Forms.Label();
            this.labelFileInformation = new System.Windows.Forms.Label();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.textBoxFileName = new System.Windows.Forms.TextBox();
            this.labelFilePath = new System.Windows.Forms.Label();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBoxOutput = new System.Windows.Forms.GroupBox();
            this.buttonConvert = new System.Windows.Forms.Button();
            this.labelFormatLabel = new System.Windows.Forms.Label();
            this.comboBoxFormat = new System.Windows.Forms.ComboBox();
            this.labelNumbersInRow = new System.Windows.Forms.Label();
            this.numericNumbersInRow = new System.Windows.Forms.NumericUpDown();
            this.buttonChooseSaveFile = new System.Windows.Forms.Button();
            this.textBoxOutFile = new System.Windows.Forms.TextBox();
            this.labelOutFileLabel = new System.Windows.Forms.Label();
            this.labelStructTypeLabel = new System.Windows.Forms.Label();
            this.comboBoxStructType = new System.Windows.Forms.ComboBox();
            this.textBoxStructName = new System.Windows.Forms.TextBox();
            this.labelStructNameLabel = new System.Windows.Forms.Label();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.labelSampleRateLabel = new System.Windows.Forms.Label();
            this.labelSampleRate = new System.Windows.Forms.Label();
            this.labelBytePerSecondLabel = new System.Windows.Forms.Label();
            this.labelBytePerSecond = new System.Windows.Forms.Label();
            this.labelBlockSize = new System.Windows.Forms.Label();
            this.labelBlockSizeLabel = new System.Windows.Forms.Label();
            this.labelDataSize = new System.Windows.Forms.Label();
            this.labelDataSizeLabel = new System.Windows.Forms.Label();
            this.groupInputInfo.SuspendLayout();
            this.groupBoxOutput.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericNumbersInRow)).BeginInit();
            this.SuspendLayout();
            // 
            // groupInputInfo
            // 
            this.groupInputInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupInputInfo.Controls.Add(this.labelDataSize);
            this.groupInputInfo.Controls.Add(this.labelDataSizeLabel);
            this.groupInputInfo.Controls.Add(this.labelBlockSize);
            this.groupInputInfo.Controls.Add(this.labelBlockSizeLabel);
            this.groupInputInfo.Controls.Add(this.labelBytePerSecond);
            this.groupInputInfo.Controls.Add(this.labelBytePerSecondLabel);
            this.groupInputInfo.Controls.Add(this.labelSampleRate);
            this.groupInputInfo.Controls.Add(this.labelSampleRateLabel);
            this.groupInputInfo.Controls.Add(this.labelWavFormat);
            this.groupInputInfo.Controls.Add(this.labelWaveFormatLabel);
            this.groupInputInfo.Controls.Add(this.labelChannels);
            this.groupInputInfo.Controls.Add(this.labelChannelsLabel);
            this.groupInputInfo.Controls.Add(this.labelRiff);
            this.groupInputInfo.Controls.Add(this.labelRFFID);
            this.groupInputInfo.Controls.Add(this.labelFileInformation);
            this.groupInputInfo.Controls.Add(this.btnSelectFile);
            this.groupInputInfo.Controls.Add(this.textBoxFileName);
            this.groupInputInfo.Controls.Add(this.labelFilePath);
            this.groupInputInfo.Location = new System.Drawing.Point(12, 1);
            this.groupInputInfo.Name = "groupInputInfo";
            this.groupInputInfo.Size = new System.Drawing.Size(688, 151);
            this.groupInputInfo.TabIndex = 0;
            this.groupInputInfo.TabStop = false;
            this.groupInputInfo.Text = " Input file ";
            // 
            // labelWavFormat
            // 
            this.labelWavFormat.AutoSize = true;
            this.labelWavFormat.Location = new System.Drawing.Point(77, 127);
            this.labelWavFormat.Name = "labelWavFormat";
            this.labelWavFormat.Size = new System.Drawing.Size(10, 13);
            this.labelWavFormat.TabIndex = 8;
            this.labelWavFormat.Text = "-";
            // 
            // labelWaveFormatLabel
            // 
            this.labelWaveFormatLabel.AutoSize = true;
            this.labelWaveFormatLabel.Location = new System.Drawing.Point(6, 127);
            this.labelWaveFormatLabel.Name = "labelWaveFormatLabel";
            this.labelWaveFormatLabel.Size = new System.Drawing.Size(55, 13);
            this.labelWaveFormatLabel.TabIndex = 7;
            this.labelWaveFormatLabel.Text = "WAV Fmt:";
            // 
            // labelChannels
            // 
            this.labelChannels.AutoSize = true;
            this.labelChannels.Location = new System.Drawing.Point(77, 105);
            this.labelChannels.Name = "labelChannels";
            this.labelChannels.Size = new System.Drawing.Size(10, 13);
            this.labelChannels.TabIndex = 6;
            this.labelChannels.Text = "-";
            // 
            // labelChannelsLabel
            // 
            this.labelChannelsLabel.AutoSize = true;
            this.labelChannelsLabel.Location = new System.Drawing.Point(6, 105);
            this.labelChannelsLabel.Name = "labelChannelsLabel";
            this.labelChannelsLabel.Size = new System.Drawing.Size(54, 13);
            this.labelChannelsLabel.TabIndex = 5;
            this.labelChannelsLabel.Text = "Channels:";
            // 
            // labelRiff
            // 
            this.labelRiff.AutoSize = true;
            this.labelRiff.Location = new System.Drawing.Point(77, 82);
            this.labelRiff.Name = "labelRiff";
            this.labelRiff.Size = new System.Drawing.Size(10, 13);
            this.labelRiff.TabIndex = 4;
            this.labelRiff.Text = "-";
            // 
            // labelRFFID
            // 
            this.labelRFFID.AutoSize = true;
            this.labelRFFID.Location = new System.Drawing.Point(6, 82);
            this.labelRFFID.Name = "labelRFFID";
            this.labelRFFID.Size = new System.Drawing.Size(47, 13);
            this.labelRFFID.TabIndex = 3;
            this.labelRFFID.Text = "RIFF ID:";
            // 
            // labelFileInformation
            // 
            this.labelFileInformation.AutoSize = true;
            this.labelFileInformation.Location = new System.Drawing.Point(6, 60);
            this.labelFileInformation.Name = "labelFileInformation";
            this.labelFileInformation.Size = new System.Drawing.Size(81, 13);
            this.labelFileInformation.TabIndex = 2;
            this.labelFileInformation.Text = "File Information:";
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectFile.Location = new System.Drawing.Point(657, 21);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(25, 23);
            this.btnSelectFile.TabIndex = 1;
            this.btnSelectFile.Text = "...";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // textBoxFileName
            // 
            this.textBoxFileName.Location = new System.Drawing.Point(80, 23);
            this.textBoxFileName.Name = "textBoxFileName";
            this.textBoxFileName.ReadOnly = true;
            this.textBoxFileName.Size = new System.Drawing.Size(571, 20);
            this.textBoxFileName.TabIndex = 1;
            // 
            // labelFilePath
            // 
            this.labelFilePath.AccessibleRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.labelFilePath.AutoSize = true;
            this.labelFilePath.Location = new System.Drawing.Point(6, 26);
            this.labelFilePath.Name = "labelFilePath";
            this.labelFilePath.Size = new System.Drawing.Size(58, 13);
            this.labelFilePath.TabIndex = 0;
            this.labelFilePath.Text = "File name: ";
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            this.openFileDialog.Filter = "\"WAV files\"|*.wav";
            // 
            // groupBoxOutput
            // 
            this.groupBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxOutput.Controls.Add(this.buttonConvert);
            this.groupBoxOutput.Controls.Add(this.labelFormatLabel);
            this.groupBoxOutput.Controls.Add(this.comboBoxFormat);
            this.groupBoxOutput.Controls.Add(this.labelNumbersInRow);
            this.groupBoxOutput.Controls.Add(this.numericNumbersInRow);
            this.groupBoxOutput.Controls.Add(this.buttonChooseSaveFile);
            this.groupBoxOutput.Controls.Add(this.textBoxOutFile);
            this.groupBoxOutput.Controls.Add(this.labelOutFileLabel);
            this.groupBoxOutput.Controls.Add(this.labelStructTypeLabel);
            this.groupBoxOutput.Controls.Add(this.comboBoxStructType);
            this.groupBoxOutput.Controls.Add(this.textBoxStructName);
            this.groupBoxOutput.Controls.Add(this.labelStructNameLabel);
            this.groupBoxOutput.Location = new System.Drawing.Point(12, 156);
            this.groupBoxOutput.Name = "groupBoxOutput";
            this.groupBoxOutput.Size = new System.Drawing.Size(688, 135);
            this.groupBoxOutput.TabIndex = 1;
            this.groupBoxOutput.TabStop = false;
            this.groupBoxOutput.Text = " Output struct ";
            // 
            // buttonConvert
            // 
            this.buttonConvert.Enabled = false;
            this.buttonConvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonConvert.Location = new System.Drawing.Point(80, 106);
            this.buttonConvert.Name = "buttonConvert";
            this.buttonConvert.Size = new System.Drawing.Size(75, 23);
            this.buttonConvert.TabIndex = 11;
            this.buttonConvert.Text = "Convert";
            this.buttonConvert.UseVisualStyleBackColor = true;
            this.buttonConvert.Click += new System.EventHandler(this.buttonConvert_Click);
            // 
            // labelFormatLabel
            // 
            this.labelFormatLabel.AutoSize = true;
            this.labelFormatLabel.Location = new System.Drawing.Point(475, 75);
            this.labelFormatLabel.Name = "labelFormatLabel";
            this.labelFormatLabel.Size = new System.Drawing.Size(42, 13);
            this.labelFormatLabel.TabIndex = 10;
            this.labelFormatLabel.Text = "Format:";
            // 
            // comboBoxFormat
            // 
            this.comboBoxFormat.Enabled = false;
            this.comboBoxFormat.FormattingEnabled = true;
            this.comboBoxFormat.Items.AddRange(new object[] {
            "Big-Endian",
            "Low-Endian"});
            this.comboBoxFormat.Location = new System.Drawing.Point(530, 72);
            this.comboBoxFormat.Name = "comboBoxFormat";
            this.comboBoxFormat.Size = new System.Drawing.Size(121, 21);
            this.comboBoxFormat.TabIndex = 9;
            // 
            // labelNumbersInRow
            // 
            this.labelNumbersInRow.AutoSize = true;
            this.labelNumbersInRow.Location = new System.Drawing.Point(475, 49);
            this.labelNumbersInRow.Name = "labelNumbersInRow";
            this.labelNumbersInRow.Size = new System.Drawing.Size(83, 13);
            this.labelNumbersInRow.TabIndex = 8;
            this.labelNumbersInRow.Text = "Numbers in row:";
            // 
            // numericNumbersInRow
            // 
            this.numericNumbersInRow.Location = new System.Drawing.Point(600, 47);
            this.numericNumbersInRow.Maximum = new decimal(new int[] {
            80,
            0,
            0,
            0});
            this.numericNumbersInRow.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericNumbersInRow.Name = "numericNumbersInRow";
            this.numericNumbersInRow.Size = new System.Drawing.Size(51, 20);
            this.numericNumbersInRow.TabIndex = 7;
            this.numericNumbersInRow.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // buttonChooseSaveFile
            // 
            this.buttonChooseSaveFile.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonChooseSaveFile.Location = new System.Drawing.Point(657, 18);
            this.buttonChooseSaveFile.Name = "buttonChooseSaveFile";
            this.buttonChooseSaveFile.Size = new System.Drawing.Size(25, 23);
            this.buttonChooseSaveFile.TabIndex = 6;
            this.buttonChooseSaveFile.Text = "...";
            this.buttonChooseSaveFile.UseVisualStyleBackColor = true;
            this.buttonChooseSaveFile.Click += new System.EventHandler(this.buttonChooseSaveFile_Click);
            // 
            // textBoxOutFile
            // 
            this.textBoxOutFile.Location = new System.Drawing.Point(80, 20);
            this.textBoxOutFile.Name = "textBoxOutFile";
            this.textBoxOutFile.Size = new System.Drawing.Size(571, 20);
            this.textBoxOutFile.TabIndex = 5;
            this.textBoxOutFile.Text = "out.txt";
            // 
            // labelOutFileLabel
            // 
            this.labelOutFileLabel.AccessibleRole = System.Windows.Forms.AccessibleRole.StaticText;
            this.labelOutFileLabel.AutoSize = true;
            this.labelOutFileLabel.Location = new System.Drawing.Point(6, 23);
            this.labelOutFileLabel.Name = "labelOutFileLabel";
            this.labelOutFileLabel.Size = new System.Drawing.Size(58, 13);
            this.labelOutFileLabel.TabIndex = 4;
            this.labelOutFileLabel.Text = "File name: ";
            // 
            // labelStructTypeLabel
            // 
            this.labelStructTypeLabel.AutoSize = true;
            this.labelStructTypeLabel.Location = new System.Drawing.Point(6, 75);
            this.labelStructTypeLabel.Name = "labelStructTypeLabel";
            this.labelStructTypeLabel.Size = new System.Drawing.Size(65, 13);
            this.labelStructTypeLabel.TabIndex = 3;
            this.labelStructTypeLabel.Text = "Struct Type:";
            // 
            // comboBoxStructType
            // 
            this.comboBoxStructType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxStructType.FormattingEnabled = true;
            this.comboBoxStructType.Items.AddRange(new object[] {
            "int16_t",
            "int8_t",
            "uint16_t"});
            this.comboBoxStructType.Location = new System.Drawing.Point(80, 72);
            this.comboBoxStructType.Name = "comboBoxStructType";
            this.comboBoxStructType.Size = new System.Drawing.Size(88, 21);
            this.comboBoxStructType.TabIndex = 2;
            // 
            // textBoxStructName
            // 
            this.textBoxStructName.Location = new System.Drawing.Point(80, 46);
            this.textBoxStructName.Name = "textBoxStructName";
            this.textBoxStructName.Size = new System.Drawing.Size(152, 20);
            this.textBoxStructName.TabIndex = 1;
            this.textBoxStructName.Text = "pcm";
            // 
            // labelStructNameLabel
            // 
            this.labelStructNameLabel.AutoSize = true;
            this.labelStructNameLabel.Location = new System.Drawing.Point(6, 49);
            this.labelStructNameLabel.Name = "labelStructNameLabel";
            this.labelStructNameLabel.Size = new System.Drawing.Size(69, 13);
            this.labelStructNameLabel.TabIndex = 0;
            this.labelStructNameLabel.Text = "Struct Name:";
            // 
            // labelSampleRateLabel
            // 
            this.labelSampleRateLabel.AutoSize = true;
            this.labelSampleRateLabel.Location = new System.Drawing.Point(244, 82);
            this.labelSampleRateLabel.Name = "labelSampleRateLabel";
            this.labelSampleRateLabel.Size = new System.Drawing.Size(71, 13);
            this.labelSampleRateLabel.TabIndex = 9;
            this.labelSampleRateLabel.Text = "Sample Rate:";
            // 
            // labelSampleRate
            // 
            this.labelSampleRate.AutoSize = true;
            this.labelSampleRate.Location = new System.Drawing.Point(340, 82);
            this.labelSampleRate.Name = "labelSampleRate";
            this.labelSampleRate.Size = new System.Drawing.Size(10, 13);
            this.labelSampleRate.TabIndex = 10;
            this.labelSampleRate.Text = "-";
            // 
            // labelBytePerSecondLabel
            // 
            this.labelBytePerSecondLabel.AutoSize = true;
            this.labelBytePerSecondLabel.Location = new System.Drawing.Point(244, 105);
            this.labelBytePerSecondLabel.Name = "labelBytePerSecondLabel";
            this.labelBytePerSecondLabel.Size = new System.Drawing.Size(93, 13);
            this.labelBytePerSecondLabel.TabIndex = 11;
            this.labelBytePerSecondLabel.Text = "Byte  Per Second:";
            // 
            // labelBytePerSecond
            // 
            this.labelBytePerSecond.AutoSize = true;
            this.labelBytePerSecond.Location = new System.Drawing.Point(340, 105);
            this.labelBytePerSecond.Name = "labelBytePerSecond";
            this.labelBytePerSecond.Size = new System.Drawing.Size(10, 13);
            this.labelBytePerSecond.TabIndex = 12;
            this.labelBytePerSecond.Text = "-";
            // 
            // labelBlockSize
            // 
            this.labelBlockSize.AutoSize = true;
            this.labelBlockSize.Location = new System.Drawing.Point(623, 82);
            this.labelBlockSize.Name = "labelBlockSize";
            this.labelBlockSize.Size = new System.Drawing.Size(10, 13);
            this.labelBlockSize.TabIndex = 14;
            this.labelBlockSize.Text = "-";
            // 
            // labelBlockSizeLabel
            // 
            this.labelBlockSizeLabel.AutoSize = true;
            this.labelBlockSizeLabel.Location = new System.Drawing.Point(527, 82);
            this.labelBlockSizeLabel.Name = "labelBlockSizeLabel";
            this.labelBlockSizeLabel.Size = new System.Drawing.Size(60, 13);
            this.labelBlockSizeLabel.TabIndex = 13;
            this.labelBlockSizeLabel.Text = "Block Size:";
            // 
            // labelDataSize
            // 
            this.labelDataSize.AutoSize = true;
            this.labelDataSize.Location = new System.Drawing.Point(623, 105);
            this.labelDataSize.Name = "labelDataSize";
            this.labelDataSize.Size = new System.Drawing.Size(10, 13);
            this.labelDataSize.TabIndex = 16;
            this.labelDataSize.Text = "-";
            // 
            // labelDataSizeLabel
            // 
            this.labelDataSizeLabel.AutoSize = true;
            this.labelDataSizeLabel.Location = new System.Drawing.Point(527, 105);
            this.labelDataSizeLabel.Name = "labelDataSizeLabel";
            this.labelDataSizeLabel.Size = new System.Drawing.Size(56, 13);
            this.labelDataSizeLabel.TabIndex = 15;
            this.labelDataSizeLabel.Text = "Data Size:";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 303);
            this.Controls.Add(this.groupBoxOutput);
            this.Controls.Add(this.groupInputInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "WavToStruct";
            this.groupInputInfo.ResumeLayout(false);
            this.groupInputInfo.PerformLayout();
            this.groupBoxOutput.ResumeLayout(false);
            this.groupBoxOutput.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericNumbersInRow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupInputInfo;
        private System.Windows.Forms.Label labelFilePath;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.TextBox textBoxFileName;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label labelFileInformation;
        private System.Windows.Forms.Label labelRFFID;
        private System.Windows.Forms.Label labelRiff;
        private System.Windows.Forms.Label labelChannels;
        private System.Windows.Forms.Label labelChannelsLabel;
        private System.Windows.Forms.Label labelWavFormat;
        private System.Windows.Forms.Label labelWaveFormatLabel;
        private System.Windows.Forms.GroupBox groupBoxOutput;
        private System.Windows.Forms.Label labelStructNameLabel;
        private System.Windows.Forms.TextBox textBoxStructName;
        private System.Windows.Forms.Label labelStructTypeLabel;
        private System.Windows.Forms.ComboBox comboBoxStructType;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.TextBox textBoxOutFile;
        private System.Windows.Forms.Label labelOutFileLabel;
        private System.Windows.Forms.Button buttonChooseSaveFile;
        private System.Windows.Forms.Label labelNumbersInRow;
        private System.Windows.Forms.NumericUpDown numericNumbersInRow;
        private System.Windows.Forms.Label labelFormatLabel;
        private System.Windows.Forms.ComboBox comboBoxFormat;
        private System.Windows.Forms.Button buttonConvert;
        private System.Windows.Forms.Label labelSampleRate;
        private System.Windows.Forms.Label labelSampleRateLabel;
        private System.Windows.Forms.Label labelBytePerSecond;
        private System.Windows.Forms.Label labelBytePerSecondLabel;
        private System.Windows.Forms.Label labelDataSize;
        private System.Windows.Forms.Label labelDataSizeLabel;
        private System.Windows.Forms.Label labelBlockSize;
        private System.Windows.Forms.Label labelBlockSizeLabel;
    }
}

