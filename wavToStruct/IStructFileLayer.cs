﻿using System;
using System.Collections.Generic;
using wavToStruct.Model;

namespace wavToStruct
{
    interface IStructFileLayer
    {
        string OutFileName { get; set; }
        string StructName { get; set; }
        int ArraySize { get; set; }
        short RowSize { get; set; }
        StructTypes StructTypes { get; set; }
        List<string> StructData { get; set; }
        bool ConvertEnabled { get; set; }
        event EventHandler<EventArgs> SaveStruct;
    }
}
